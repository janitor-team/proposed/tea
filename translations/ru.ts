<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CAboutWindow</name>
    <message>
        <location filename="../rvln.cpp" line="4893"/>
        <source>Code</source>
        <translation>Код</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="4894"/>
        <source>Acknowledgements</source>
        <translation>Благодарности</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="4895"/>
        <source>Translations</source>
        <translation>Переводы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="4896"/>
        <source>Packages</source>
        <translation>Пакеты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="4904"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
</context>
<context>
    <name>CDarkerWindow</name>
    <message>
        <location filename="../rvln.cpp" line="8639"/>
        <source>Darker palette</source>
        <translation>Палитра темнее</translation>
    </message>
</context>
<context>
    <name>CDocument</name>
    <message>
        <location filename="../document.cpp" line="130"/>
        <source>cannot open %1 because of: %2</source>
        <translation>Не могу открыть %1, ибо %2</translation>
    </message>
    <message>
        <location filename="../document.cpp" line="186"/>
        <source>cannot save %1 because of: %2</source>
        <translation>не могу сохранить %1, ибо %2</translation>
    </message>
    <message>
        <location filename="../document.cpp" line="199"/>
        <source>%1 is saved</source>
        <translation>%1 сохранён</translation>
    </message>
    <message>
        <location filename="../document.cpp" line="250"/>
        <source>%1 has been modified.
Do you want to save your changes?</source>
        <translation>%1 был изменен. Желаете сохранить изменения?</translation>
    </message>
    <message>
        <location filename="../document.cpp" line="151"/>
        <source>%1 is open</source>
        <translation>%1 открыт</translation>
    </message>
    <message>
        <location filename="../document.cpp" line="216"/>
        <source>new[%1]</source>
        <translation>новый[%1]</translation>
    </message>
</context>
<context>
    <name>CFontBox</name>
    <message>
        <location filename="../fontbox.cpp" line="28"/>
        <source>Example string</source>
        <translation>Строка примера</translation>
    </message>
    <message>
        <location filename="../fontbox.cpp" line="33"/>
        <source>Font gallery</source>
        <translation>Галерея шрифтов</translation>
    </message>
</context>
<context>
    <name>CImgViewer</name>
    <message>
        <location filename="../img_viewer.cpp" line="30"/>
        <source>preview</source>
        <translation>просмотр</translation>
    </message>
</context>
<context>
    <name>CTioReadOnly</name>
    <message>
        <source>saving of this format is not supported</source>
        <translation type="vanished">сохранение этого формата не поддерживается</translation>
    </message>
    <message>
        <location filename="../tio.cpp" line="270"/>
        <source>Saving for this format is not supported</source>
        <translation>Сохранение в этот формат не поддерживается</translation>
    </message>
</context>
<context>
    <name>CTodo</name>
    <message>
        <location filename="../todo.cpp" line="38"/>
        <source>Attention! Attention!</source>
        <translation>Внимание! Внимание!</translation>
    </message>
</context>
<context>
    <name>CViewerWindow</name>
    <message>
        <source>preview</source>
        <translation type="obsolete">просмотр</translation>
    </message>
</context>
<context>
    <name>CZORWindow</name>
    <message>
        <source> scaled to</source>
        <translation type="obsolete">масштабировано к </translation>
    </message>
    <message>
        <location filename="../img_viewer.cpp" line="460"/>
        <source>scaled to: </source>
        <translation>масштабировано к:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../fman.cpp" line="224"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../fman.cpp" line="225"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../fman.cpp" line="226"/>
        <source>Modified at</source>
        <translation>Изменено</translation>
    </message>
</context>
<context>
    <name>QuaZipFile</name>
    <message>
        <location filename="../quazipfile.cpp" line="247"/>
        <source>ZIP/UNZIP API error %1</source>
        <translation>ZIP/UNZIP ошибка API %1</translation>
    </message>
</context>
<context>
    <name>rvln</name>
    <message>
        <location filename="../rvln.cpp" line="1162"/>
        <source>Create a new file</source>
        <translation>Создать новый файл</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1170"/>
        <source>Open an existing file</source>
        <translation>Открыть существующий файл</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1178"/>
        <source>Save the document to disk</source>
        <translation>Сохранить документ на диск</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1182"/>
        <source>Save the document under a new name</source>
        <translation>Сохранить документ под именем</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1187"/>
        <source>Exit the application</source>
        <translation>Выйти из программы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1195"/>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation>Вырезать текущее выделение в буфер обмена</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1204"/>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation>Копировать текущее выделение в буфер обмена</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1213"/>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation>Вставить из буфера обмена</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1241"/>
        <source>Open at cursor</source>
        <translation>Открыть под курсором</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1253"/>
        <source>Save .bak</source>
        <translation>Сохранить запасную копию</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1374"/>
        <source>Insert image</source>
        <translation>Вставить картинку</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1391"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1392"/>
        <source>Find next</source>
        <translation>Найти дальше</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1393"/>
        <source>Find previous</source>
        <translation>Найти назад</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1468"/>
        <source>Case</source>
        <translation>Регистр</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1233"/>
        <location filename="../rvln.cpp" line="1772"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1297"/>
        <location filename="../rvln.cpp" line="1778"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="vanished">Готово</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2473"/>
        <source>Editor font</source>
        <translation>Шрифт редактора</translation>
    </message>
    <message>
        <source>UI style</source>
        <translation type="vanished">Стиль интерфейса</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2554"/>
        <source>Word wrap</source>
        <translation>Перенос строк</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="954"/>
        <location filename="../rvln.cpp" line="1086"/>
        <location filename="../rvln.cpp" line="5783"/>
        <location filename="../rvln.cpp" line="6438"/>
        <source>Charset</source>
        <translation>Кодировка</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1150"/>
        <source>Test</source>
        <translation>Проверка</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1159"/>
        <source>New</source>
        <translation>Новый</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1239"/>
        <location filename="../rvln.cpp" line="5807"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1176"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="418"/>
        <location filename="../rvln.cpp" line="1805"/>
        <source>FIF</source>
        <translation>ЗПВ</translation>
    </message>
    <message>
        <source>&lt;b&gt;TEA %1&lt;/b&gt; by Peter Semiletov - tea@list.ru&lt;br&gt;site 1: http://semiletov.org/tea&lt;br&gt;site 2: http://tea.ourproject.org&lt;br&gt;read the Manual under the &lt;i&gt;Learn&lt;/i&gt; tab!</source>
        <translation type="obsolete">&lt;b&gt;TEA %1&lt;/b&gt; от Петра Семилетова - tea@list.ru&lt;br&gt;сайт 1: http://semiletov.org/tea&lt;br&gt;сайт 2: http://tea.ourproject.org&lt;br&gt;читайте Руководство на вкладке &lt;i&gt;Узнать&lt;/i&gt;!</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1181"/>
        <source>Save As</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1185"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1193"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1202"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1211"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1216"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1220"/>
        <source>Redo</source>
        <translation>Переделать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1223"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1226"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1242"/>
        <source>Crapbook</source>
        <translation>Фигня</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1268"/>
        <source>Recent files</source>
        <translation>Последние файлы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1289"/>
        <source>Print</source>
        <translation>Печатать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1339"/>
        <source>Markup</source>
        <translation>Вёрстка</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1388"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1436"/>
        <location filename="../rvln.cpp" line="2880"/>
        <source>Functions</source>
        <translation>Функции</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1504"/>
        <source>Filter with regexp</source>
        <translation>Фильтровать по регэкспу</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1547"/>
        <source>Apply to each line</source>
        <translation>Применить к каждой строке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1554"/>
        <source>Reverse</source>
        <translation>Перевернуть</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1471"/>
        <source>UPCASE</source>
        <translation>ВЕРХНИЙ</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1472"/>
        <source>lower case</source>
        <translation>нижний</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1577"/>
        <source>Add to dictionary</source>
        <translation>Добавить в словарь</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1576"/>
        <source>Suggest</source>
        <translation>Предположить</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1575"/>
        <source>Spell check</source>
        <translation>Проверить правописание</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1572"/>
        <source>Spell-checker languages</source>
        <translation>Языки проверки правописания</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1630"/>
        <source>Nav</source>
        <translation>Нав</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1633"/>
        <source>Save position</source>
        <translation>Запомнить место</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1634"/>
        <source>Go to saved position</source>
        <translation>К сохранённому месту</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1635"/>
        <source>Go to line</source>
        <translation>К строке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1731"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Помощь</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2988"/>
        <source>Assign</source>
        <translation>Назначить</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2989"/>
        <source>Remove</source>
        <translation>Удалить привязку</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3003"/>
        <source>Keyboard</source>
        <translation>Клавиатура</translation>
    </message>
    <message>
        <source>edit</source>
        <translation type="obsolete">править</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1250"/>
        <source>Save as different</source>
        <translation>Сохранить иначе</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1254"/>
        <source>Save timestamped version</source>
        <translation>Сохранить версию по времени</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1276"/>
        <source>Templates</source>
        <translation>Шаблоны</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1270"/>
        <source>Bookmarks</source>
        <translation>Закладки</translation>
    </message>
    <message>
        <source>&lt;b&gt;TEA %1 @ http://tea-editor.sourceforge.net&lt;/b&gt;&lt;br&gt;by Peter Semiletov (peter.semiletov@gmail.com)&lt;br&gt;read the Manual under the &lt;i&gt;Learn&lt;/i&gt; tab!</source>
        <translation type="obsolete">&lt;b&gt;TEA %1 @ http://tea-editor.sourceforge.net&lt;/b&gt;&lt;br&gt;разработка Петра Семилетова (peter.semiletov@gmail.com)&lt;br&gt;читайте руководство на вкладке &lt;i&gt;Узнать&lt;/i&gt;!</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="952"/>
        <source>All (*);;Text files (*.txt);;Markup files (*.xml *.html *.htm *.);;C/C++ (*.c *.h *.cpp *.hh *.c++ *.h++ *.cxx)</source>
        <translation>Все (*);;Текстовые файлы (*.txt);;Файлы разметки (*.xml *.html *.htm *.);;C/C++ (*.c *.h *.cpp *.hh *.c++ *.h++ *.cxx)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1156"/>
        <location filename="../rvln.cpp" line="1641"/>
        <location filename="../rvln.cpp" line="2767"/>
        <source>Labels</source>
        <translation>Метки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1243"/>
        <source>Notes</source>
        <translation>Заметки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1263"/>
        <source>Set UNIX end of line</source>
        <translation>Установить конец строки как в UNIX</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1264"/>
        <source>Set Windows end of line</source>
        <translation>Установить конец строки как в Windows</translation>
    </message>
    <message>
        <source>Set traditional Mac end of line</source>
        <translation type="obsolete">Установить конец строки как в Mac</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1272"/>
        <source>Edit bookmarks</source>
        <translation>Править закладки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1273"/>
        <source>Add to bookmarks</source>
        <translation>Добавить в закладки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1274"/>
        <source>Find obsolete paths</source>
        <translation>Найти ошибочные пути</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1292"/>
        <source>Close current</source>
        <translation>Закрыть текущий файл</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1314"/>
        <source>Copy current file name</source>
        <translation>Копировать имя текущего файла</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1325"/>
        <source>Indent by first line</source>
        <translation>Отступ по первой строке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1329"/>
        <source>Comment selection</source>
        <translation>Закомментировать выделенное</translation>
    </message>
    <message>
        <source>Variants</source>
        <translation type="obsolete">Варианты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1333"/>
        <source>Set as storage file</source>
        <translation>Установить как файл хранилища</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1334"/>
        <source>Copy to storage file</source>
        <translation>Копировать в файл хранилища</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1335"/>
        <source>Start/stop capture clipboard to storage file</source>
        <translation>Захватить/нет буфер обмена в файл хранилища</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1342"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1349"/>
        <source>Header</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1356"/>
        <source>Align</source>
        <translation>Выравнивание</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1359"/>
        <source>Center</source>
        <translation>Середина</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1362"/>
        <source>Justify</source>
        <translation>По ширине</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1376"/>
        <source>[X]HTML tools</source>
        <translation>Инструменты [X]HTML</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1379"/>
        <source>Text to [X]HTML</source>
        <translation>Из текста в [X]HTML</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1380"/>
        <source>Convert tags to entities</source>
        <translation>Перевести тэги в сущности</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1381"/>
        <source>Antispam e-mail</source>
        <translation>Кодировать адрес e-mail супротив спама</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1382"/>
        <source>Document weight</source>
        <translation>Взвесить документ</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1384"/>
        <source>Strip HTML tags</source>
        <translation>Очистить от HTML-тэгов</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1385"/>
        <source>Rename selected file</source>
        <translation>Переименовать выделенный файл</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1397"/>
        <source>Find in files</source>
        <translation>Найти в файлах</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1401"/>
        <source>Replace with</source>
        <translation>Заменить на</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1402"/>
        <source>Replace all</source>
        <translation>Заменить всё</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1403"/>
        <source>Replace all in opened files</source>
        <translation>Заменить всё в открытых файлах</translation>
    </message>
    <message>
        <source>Mark all</source>
        <translation type="obsolete">Пометить всё</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1408"/>
        <source>Unmark</source>
        <translation>Снять пометки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1421"/>
        <source>From cursor</source>
        <translation>От курсора</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1426"/>
        <source>Regexp mode</source>
        <translation>В режиме регулярных выражений</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1431"/>
        <source>Fuzzy mode</source>
        <translation>Нечеткое условие</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1439"/>
        <source>Repeat last</source>
        <translation>Повторить последнее</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1441"/>
        <source>Tools</source>
        <translation>Инструменты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1444"/>
        <source>Scale image</source>
        <translation>Масштабировать картинку</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1448"/>
        <source>Plugins</source>
        <translation>Плагины</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1451"/>
        <source>Snippets</source>
        <translation>Сниппеты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1453"/>
        <source>Tables</source>
        <translation>Таблицы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1459"/>
        <source>TEA project template</source>
        <translation>Шаблон проекта TEA</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1461"/>
        <source>HTML5 template</source>
        <translation>Шаблон HTML5</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1462"/>
        <source>C++ template</source>
        <translation>Шаблон C++</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1463"/>
        <source>C template</source>
        <translation>Шаблон C</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1464"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1465"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1475"/>
        <source>Sort</source>
        <translation>Сортировка</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1478"/>
        <source>Sort case sensitively</source>
        <translation>Сортировать учитывая регистр</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1480"/>
        <source>Sort case sensitively, with separator</source>
        <translation>Сортировать зависимо от регистра, по разделителю</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1481"/>
        <source>Flip a list</source>
        <translation>Перевернуть список</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1482"/>
        <source>Flip a list with separator</source>
        <translation>Перевернуть список, размежеванный разделителями</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1485"/>
        <source>Cells</source>
        <translation>Ячейки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1488"/>
        <source>Sort table by column ABC</source>
        <translation>Упорядочить таблицу по столбцу в алфавитном порядке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1489"/>
        <source>Swap cells</source>
        <translation>Поменять местами ячейки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1490"/>
        <source>Delete by column</source>
        <translation>Удалить по столбцу</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1491"/>
        <source>Copy by column[s]</source>
        <translation>Копировать по столбцу(цам)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1494"/>
        <source>Filter</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1497"/>
        <source>Remove duplicates</source>
        <translation>Удалить повторения</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1498"/>
        <source>Remove empty lines</source>
        <translation>Удалить пустые строки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1499"/>
        <source>Remove lines &lt; N size</source>
        <translation>Удалить строки &lt; размера N</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1500"/>
        <source>Remove lines &gt; N size</source>
        <translation>Удалить строки &gt; размера N</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1501"/>
        <source>Remove before delimiter at each line</source>
        <translation>Удалить перед разделителем в каждой строке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1502"/>
        <source>Remove after delimiter at each line</source>
        <translation>Удалить после разделителя в каждой строке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1514"/>
        <source>Decimal to binary</source>
        <translation>Десятичное в двоичное</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1515"/>
        <source>Binary to decimal</source>
        <translation>Двоичное в десятичное</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1516"/>
        <source>Flip bits (bitwise complement)</source>
        <translation>Перевернуть биты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1518"/>
        <source>Sum by last column</source>
        <translation>Сложить по последнему столбцу</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1519"/>
        <source>deg min sec &gt; dec degrees</source>
        <translation>Град мин сек &gt; дес град</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1520"/>
        <source>dec degrees &gt; deg min sec</source>
        <translation>Дес град &gt; град мин сек</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1523"/>
        <source>Morse code</source>
        <translation>Азбука Морзе</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1526"/>
        <source>From Russian to Morse</source>
        <translation>Из русского в морзянку</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1527"/>
        <source>From Morse To Russian</source>
        <translation>Из морзянки в русский</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1528"/>
        <source>From English to Morse</source>
        <translation>С английского в код Морзе</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1529"/>
        <source>From Morse To English</source>
        <translation>Из кода Морзе на английский</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1535"/>
        <source>Text statistics</source>
        <translation>Статистика по тексту</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1536"/>
        <source>Extract words</source>
        <translation>Извлечь слова</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1537"/>
        <source>Words lengths</source>
        <translation>Длины слов</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1549"/>
        <source>Remove formatting at each line</source>
        <translation>Удалить форматирование в каждой строке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1550"/>
        <source>Compress</source>
        <translation>Сжать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1551"/>
        <source>Anagram</source>
        <translation>Анаграмма</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1555"/>
        <source>Compare two strings</source>
        <translatorcomment>Сравнить два слова</translatorcomment>
        <translation>Сравнить два слова</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1609"/>
        <source>IDE</source>
        <translation>ИДЕ</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1612"/>
        <source>Run program</source>
        <translation>Запустить программу</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1613"/>
        <source>Build program</source>
        <translation>Собрать программу</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1614"/>
        <source>Clean program</source>
        <translation>Очистить сборку</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1650"/>
        <source>Multi-rename</source>
        <translation>Переименование</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1653"/>
        <source>Zero pad file names</source>
        <translation>Добавить нули к именам</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1654"/>
        <source>Delete N first chars at file names</source>
        <translation>Удалить первые N символов из имен</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1655"/>
        <source>Replace in file names</source>
        <translation>Заменить в именах файлов</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1656"/>
        <source>Apply template</source>
        <translation>Применить шаблон</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1673"/>
        <source>Checksum</source>
        <translation>Проверочная сумма</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1734"/>
        <source>Themes</source>
        <translation>Темы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2127"/>
        <source>not found!</source>
        <translation>не найдено!</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2417"/>
        <source>options</source>
        <translation>наладка</translation>
    </message>
    <message>
        <source>UI style (restart needed)</source>
        <translation type="vanished">Стиль интерфейса (требуется перезапуск)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2476"/>
        <source>Logmemo font</source>
        <translation>Шрифт логмемо</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2497"/>
        <source>GUI tabs align</source>
        <translation>Выравнивание вкладок интерфейса</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2536"/>
        <source>TEA program icon</source>
        <translation>Иконка программы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2544"/>
        <source>FIF at the top (restart needed)</source>
        <translation>ЗПВ наверху (нужен перезапуск)</translation>
    </message>
    <message>
        <source>Use colored console output (can crash)</source>
        <translation type="vanished">Цветной вывод команд в консоль (может глючить)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2558"/>
        <source>Syntax highlighting enabled</source>
        <translation>Подсветка синтаксиса включена</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2604"/>
        <source>Cursor width</source>
        <translation>Ширина курсора</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2622"/>
        <source>Show full path at window title</source>
        <translation>Показывать полный путь в заголовке окна</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2641"/>
        <source>Use Alt key to access main menu</source>
        <translation>Использовать клавишу Alt для доступа к главному меню</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2647"/>
        <source>Use Left Alt + WASD as additional cursor keys</source>
        <translation>Использовать левый Alt - WASD как доп. курсорные</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2653"/>
        <source>Use joystick as cursor keys</source>
        <translation>Использовать джойстик как курсорные клавиши</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2911"/>
        <source>Apply hard rotation by EXIF data</source>
        <translation>Применять вращение согласно данным EXIF</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2944"/>
        <source>EXIF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2949"/>
        <source>Use EXIF orientation at image viewer</source>
        <translation>Использовать ориентацию из EXIF в смотрелке картинок</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3247"/>
        <source>manual</source>
        <translation>руководство</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="4592"/>
        <source>#external programs list. example:
ff=firefox file:///%s</source>
        <translation>#список внешних приграмм. например:
ff=firefox file:///%s</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="4630"/>
        <source>Save the file first!</source>
        <translation>Сначала сохраните файл!</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5786"/>
        <source>Guess encoding!</source>
        <translation>Предположить кодировку!</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5897"/>
        <source>files</source>
        <translation>файлы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5925"/>
        <source>It seems that %1 contains TEA plugin.
 Do you want to install it?</source>
        <translation>Кажется, что %1 содержит плагин для TEA. Установить его?</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5934"/>
        <source>Cannot unzip and install plugin</source>
        <translation>Не могу распаковать и установить плагин</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6111"/>
        <source>SHA-1 checksum for %1 is %2</source>
        <translation>Проверочная сумма SHA-1 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6126"/>
        <source>SHA-2 SHA-224 checksum for %1 is %2</source>
        <translation>Проверочная сумма SHA-2 SHA-224 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6140"/>
        <source>SHA-2 SHA-384 checksum for %1 is %2</source>
        <translation>Проверочная сумма SHA-2 SHA-384 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6154"/>
        <source>SHA-2 SHA-256 checksum for %1 is %2</source>
        <translation>Проверочная сумма SHA-2 SHA-256 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6168"/>
        <source>SHA-2 SHA-512 checksum for %1 is %2</source>
        <translation>Проверочная сумма SHA-2 SHA-512 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6187"/>
        <source>SHA-3 224 checksum for %1 is %2</source>
        <translation>Проверочная сумма SHA-3 224 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6204"/>
        <source>SHA-3 256 checksum for %1 is %2</source>
        <translation>Проверочная сумма SHA-3 256 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6221"/>
        <source>SHA-3 384 checksum for %1 is %2</source>
        <translation>Проверочная сумма SHA-3 384 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6238"/>
        <source>SHA-3 512 checksum for %1 is %2</source>
        <translation>Проверочная сумма SHA-3 512 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6255"/>
        <source>Keccak 224 checksum for %1 is %2</source>
        <translation>Проверочная сумма Keccak 224 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6272"/>
        <source>Keccak 256 checksum for %1 is %2</source>
        <translation>Проверочная сумма Keccak 256 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6289"/>
        <source>Keccak 384 checksum for %1 is %2</source>
        <translation>Проверочная сумма Keccak 384 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6306"/>
        <source>Keccak 512 checksum for %1 is %2</source>
        <translation>Проверочная сумма Keccak 512 для %1 is %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6364"/>
        <source>last modified: %1</source>
        <translation>правилось: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7789"/>
        <source>Enca is not installed, falling back to the built-in detection</source>
        <translation>Enca не установлена, откатываюсь ко встроенному алгоритму определения</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8728"/>
        <source>put your notes (for this file) here and they will be saved automatically</source>
        <translation>пишите свои заметки (к этому файлу) здесь, и они будут автоматически сохранены</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8861"/>
        <source>There is no stylesheet file</source>
        <translation>Нет файла стилей</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="9443"/>
        <source>sum: %1</source>
        <translation>сумма: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="9581"/>
        <source>Incorrect parameters at FIF</source>
        <translation>Неправильные параметры в FIF</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="9653"/>
        <source>Cannot save: %1</source>
        <translation>Не могу сохранить: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="9655"/>
        <source>Saved: %1</source>
        <translation>Сохранено: %1</translation>
    </message>
    <message>
        <source>put your notes here and they will be saved automatically</source>
        <translation type="obsolete">пишите свои заметки к файлу здесь, и они будут автоматически сохранены</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8955"/>
        <source>There is no plugin file</source>
        <translation>Нет файла плагина</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8963"/>
        <source>&lt;b&gt;Error:&lt;/b&gt; </source>
        <translation>&lt;b&gt;Ошибка:&lt;/b&gt; </translation>
    </message>
    <message>
        <source>variants</source>
        <translation type="obsolete">варианты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8696"/>
        <source>Word length: </source>
        <translation>Длина слова:</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8697"/>
        <source>Number:</source>
        <translation>Количество:</translation>
    </message>
    <message>
        <source>variants file %1 is not exist</source>
        <translation type="obsolete">файл вариантов %1 не существует</translation>
    </message>
    <message>
        <source>put your notes here</source>
        <translation type="obsolete">здесь мож</translation>
    </message>
    <message>
        <source>Double quotes to TeX double quotes</source>
        <translation type="obsolete">Двойные кавычки в TeX двойные кавычки</translation>
    </message>
    <message>
        <source>Double quotes to TeX Russian quotes &lt;&lt; &gt;&gt;</source>
        <translation type="obsolete">Двойные кавычки в TeX русские кавычки &lt;&lt; &gt;&gt;</translation>
    </message>
    <message>
        <source>Double quotes to TeX Russian quotes</source>
        <translation type="obsolete">Двойные кавычки в TeX русские кавычки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1588"/>
        <source>Moon mode on/off</source>
        <translation>Лунный режим вкл/выкл</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1589"/>
        <source>Mark first date</source>
        <translation>Пометить первую дату</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1590"/>
        <source>Mark last date</source>
        <translation>Пометить последнюю дату</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1601"/>
        <source>Go to current date</source>
        <translation>Перейти к текущей дате</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1602"/>
        <source>Calculate moon days between dates</source>
        <translation>Вычислить лунные дни между датами</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1603"/>
        <source>Number of days between two dates</source>
        <translation>Количество дней между двумя датами</translation>
    </message>
    <message>
        <source>Instr</source>
        <translation type="obsolete">Инстр</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1443"/>
        <source>Font gallery</source>
        <translation>Галерея шрифтов</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1642"/>
        <source>Refresh labels</source>
        <translation>Обновить метки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1644"/>
        <source>Current files</source>
        <translation>Текущие файлы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1712"/>
        <source>List ZIP content</source>
        <translation>Показать содержимое ZIP</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1713"/>
        <source>Unpack ZIP to current directory</source>
        <translation>Распаковать ZIP в текущий каталог</translation>
    </message>
    <message>
        <source>Add user font</source>
        <translation type="vanished">Добавить пользовательский шрифт</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2661"/>
        <source>Old syntax hl engine (restart TEA to apply)</source>
        <translation>Старый движок подсветки синтаксиса (нужен перезапуск TEA)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2679"/>
        <source>Use external image viewer for F2</source>
        <translation>Использовать внешнюю смотрелку картинок для F2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2700"/>
        <source>Northern hemisphere</source>
        <translation>Северное полушарие</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2709"/>
        <source>Moon phase algorithm</source>
        <translation>Алгоритм фазы луны</translation>
    </message>
    <message>
        <source>UI tabs align</source>
        <translation type="obsolete">Положение вкладок интерфейса</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2487"/>
        <source>Up</source>
        <translation>Вверху</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1361"/>
        <location filename="../rvln.cpp" line="2490"/>
        <source>Right</source>
        <translation>Справа</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2868"/>
        <source>Fuzzy search factor</source>
        <translation>Коэффициент поиска по нечеткому условию</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3282"/>
        <source>snippet %1 is not exists</source>
        <translation>сниппет %1 не существует</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7256"/>
        <location filename="../rvln.cpp" line="7280"/>
        <source>Open Directory</source>
        <translation>Открыть каталог</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8300"/>
        <source> is unpacked</source>
        <translation>распакованы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8458"/>
        <source>Trigonometric 2</source>
        <translation>Тригонометрический 2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8459"/>
        <source>Trigonometric 1</source>
        <translation>Тригонометрический 1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8460"/>
        <source>Conway</source>
        <translation>Конуэй</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8462"/>
        <source>Leueshkanov</source>
        <translation>Леушканов</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1360"/>
        <location filename="../rvln.cpp" line="2489"/>
        <source>Left</source>
        <translation>Слева</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2510"/>
        <source>Documents tabs align</source>
        <translation>Положение вкладок документов</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2488"/>
        <source>Bottom</source>
        <translation>Снизу</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2721"/>
        <source>ZIP unpacking: file names charset</source>
        <translation>ZIP-распаковка: кодировка имен файлов</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2727"/>
        <source>ZIP packing: file names charset</source>
        <translation>ZIP-упаковка: кодировка имен файлов</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2771"/>
        <source>Label starts with: </source>
        <translation>Метка начинается с:</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2772"/>
        <source>Label ends with: </source>
        <translation>Метка завершается:</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2981"/>
        <source>Shortcut</source>
        <translation>Сочетание клавиш</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="4020"/>
        <location filename="../rvln.cpp" line="7198"/>
        <source>%1 is processed and saved</source>
        <translation>%1 обработан и сохранен</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8112"/>
        <source>Select the file name first!</source>
        <translation>Выделите сначала имя файла!</translation>
    </message>
    <message>
        <source>Select font</source>
        <translation type="vanished">Выберите шрифт</translation>
    </message>
    <message>
        <source>Fonts (*.ttf *.otf)</source>
        <translation type="vanished">Шрифты (*.ttf *.otf)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1585"/>
        <source>Calendar</source>
        <translation>Календарь</translation>
    </message>
    <message>
        <source>&lt;b&gt;TEA %1&lt;/b&gt; by Peter Semiletov, tea@list.ru&lt;br&gt;site 1: http://semiletov.org/tea&lt;br&gt;site 2: http://tea.ourproject.org&lt;br&gt;site 3 (development): https://github.com/psemiletov/tea-qt&lt;br&gt;read the Manual under the &lt;i&gt;Learn&lt;/i&gt; tab!</source>
        <translation type="obsolete">&lt;b&gt;TEA %1&lt;/b&gt; Петр Семилетов, tea@list.ru&lt;br&gt;site 1: http://semiletov.org/tea&lt;br&gt;site 2: http://tea.ourproject.org&lt;br&gt;site 3 (разработка): https://github.com/psemiletov/tea-qt&lt;br&gt;читайте руководство на вкладке &lt;i&gt;Узнать&lt;/i&gt;!</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1564"/>
        <source>LaTeX: Straight to curly double quotes</source>
        <translation>LaTeX: прямые двойные в нижние и верхние</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1565"/>
        <source>LaTeX: Straight to double angle quotes</source>
        <translation>LaTeX: прямые двойные в елочкой вар.1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1566"/>
        <source>LaTeX: Straight to double angle quotes v2</source>
        <translation>LaTeX: прямые двойные в елочкой вар.2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1592"/>
        <source>Add or subtract</source>
        <translation>Добавить или вычесть</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1595"/>
        <source>Days</source>
        <translation>Дни</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1596"/>
        <source>Months</source>
        <translation>Месяцы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1597"/>
        <source>Years</source>
        <translation>Годы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1604"/>
        <source>Remove day record</source>
        <translation>Удалить данные о дне</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2697"/>
        <source>Start week on Sunday</source>
        <translation>Неделя начинается в воскресенье</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2861"/>
        <location filename="../rvln.cpp" line="2891"/>
        <source>Miscellaneous</source>
        <translation>Разное</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6343"/>
        <source>End of line: </source>
        <translation>Конец строки: </translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7532"/>
        <source>Getting files list...</source>
        <translation>Получение списка файлов...</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7538"/>
        <source>Searching...</source>
        <translation>Ищу...</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7563"/>
        <source>cannot open %1 because of: %2</source>
        <translation>Не могу открыть %1, ибо %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7579"/>
        <source>Search results</source>
        <translation>Итоги поиска</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1153"/>
        <location filename="../rvln.cpp" line="1790"/>
        <location filename="../rvln.cpp" line="7579"/>
        <source>Files</source>
        <translation>Файлы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7642"/>
        <source>new_profile</source>
        <translation>новый профиль</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7745"/>
        <source>There are %1 lines at %2 files</source>
        <translation>%1 строк в %2 файлах</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7873"/>
        <source>Enter your daily notes here.
To use time-based reminders, specify the time signature in 24-hour format [hh:mm], i.e.:
[06:00]good morning!
[20:10]go to theatre</source>
        <translation>Введите тут свои заметки.
Чтобы включить напоминалку, используйте временные пометки в 24-часовом формате [чч:мм], то есть:
[06:00]доброе утро!
[20:10]выйти за хлебом, когда всё закрыто</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7881"/>
        <source>Enter your daily notes here.</source>
        <translation>Можете вписать тут свои заметки.</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1511"/>
        <source>Evaluate</source>
        <translation>Вычислить</translation>
    </message>
    <message>
        <source>&lt;b&gt;TEA %1&lt;/b&gt; by Peter Semiletov, tea@list.ru&lt;br&gt;sites: semiletov.org/tea and tea.ourproject.org&lt;br&gt;development: github.com/psemiletov/tea-qt&lt;br&gt;VK: vk.com/teaeditor&lt;br&gt;read the Manual under the &lt;i&gt;Learn&lt;/i&gt; tab!</source>
        <translation type="obsolete">&lt;b&gt;TEA %1&lt;/b&gt; Петр Семилетов, tea@list.ru&lt;br&gt;сайты: semiletov.org/tea и tea.ourproject.org&lt;br&gt;разработка: github.com/psemiletov/tea-qt&lt;br&gt;ВК: vk.com/teaeditor&lt;br&gt;читайте руководство на вкладке &lt;i&gt;Познать&lt;/i&gt;!</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1636"/>
        <source>Next tab</source>
        <translation>Следующая вкладка</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1637"/>
        <source>Prev tab</source>
        <translation>Предыдущая вкладка</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1618"/>
        <source>Toggle header/source</source>
        <translation>Переключить заголовок/исходник</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1639"/>
        <source>Focus the editor</source>
        <translation>Фокус в редактор</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2470"/>
        <source>Interface font</source>
        <translation>Шрифт интерфейса</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2522"/>
        <source>Icons size</source>
        <translation>Размер иконок</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2782"/>
        <source>Date format</source>
        <translation>Формат даты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2783"/>
        <source>Time format</source>
        <translation>Формат времени</translation>
    </message>
    <message>
        <source>learn</source>
        <translation type="obsolete">узнать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="7916"/>
        <source>dates</source>
        <translation>даты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8262"/>
        <source>%1 kbytes %2 &lt;br&gt;</source>
        <translation>%1 килобайт %2 &lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8266"/>
        <source>Total size = %1 kbytes in %2 files&lt;br&gt;</source>
        <translation>Общий вес = %1 килобайт в %2 файлах&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1108"/>
        <location filename="../rvln.cpp" line="5001"/>
        <source>%1 already exists
Do you want to overwrite?</source>
        <translation>%1 уже существует
Желаете перезаписать?</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1508"/>
        <source>Math</source>
        <translation>Математика</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1512"/>
        <source>Arabic to Roman</source>
        <translation>Арабское в римское</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2583"/>
        <source>Show cursor position</source>
        <translation>Отображать положение курсора</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3723"/>
        <source>%1 - saved</source>
        <translation>%1 - сохранён</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3725"/>
        <source>Cannot save %1</source>
        <translation>Не могу сохранить %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1279"/>
        <source>Configs</source>
        <translation>Конфиги</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1280"/>
        <source>Bookmarks list</source>
        <translation>Список закладок</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1281"/>
        <source>Programs list</source>
        <translation>Список программ</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1460"/>
        <source>HTML template</source>
        <translation>Шаблон HTML</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1607"/>
        <source>Run</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2328"/>
        <source>%1 is saved</source>
        <translation>%1 был сохранен </translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1455"/>
        <source>Place</source>
        <translation>Поместить</translation>
    </message>
    <message>
        <source>The famous input field. Use for search/replace, function parameters.</source>
        <translation type="vanished">Знаменитое поле ввода. Используйте для поиска и замены, а также параметров функций</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="431"/>
        <location filename="../rvln.cpp" line="535"/>
        <source>editor</source>
        <translation>правка</translation>
    </message>
    <message>
        <source>&lt;b&gt;TEA %1&lt;/b&gt; by Peter Semiletov, tea@list.ru&lt;br&gt;sites: semiletov.org/tea and tea.ourproject.org&lt;br&gt;development: github.com/psemiletov/tea-qt&lt;br&gt;VK: vk.com/teaeditor&lt;br&gt;read the Manual under the &lt;i&gt;Manual&lt;/i&gt; tab!</source>
        <translation type="vanished">&lt;b&gt;TEA %1&lt;/b&gt; Петр Семилетов, tea@list.ru&lt;br&gt;офсайты: semiletov.org/tea и tea.ourproject.org&lt;br&gt;разработка: github.com/psemiletov/tea-qt&lt;br&gt;ВК-сообщество: vk.com/teaeditor&lt;br&gt;читайте Руководство под одноименной вкладкой справа!</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1364"/>
        <source>Bold</source>
        <translation>Жирный</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1365"/>
        <source>Italic</source>
        <translation>Наклонный</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1368"/>
        <source>Link</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1369"/>
        <source>Paragraph</source>
        <translation>Параграф</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1370"/>
        <source>Color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1372"/>
        <source>Break line</source>
        <translation>Перенос строки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1373"/>
        <source>Non-breaking space</source>
        <translation>Неразрывный пробел</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1513"/>
        <source>Roman to Arabic</source>
        <translation>Римское в арабское</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1544"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1548"/>
        <source>Remove formatting</source>
        <translation>Удалить форматирование</translation>
    </message>
    <message>
        <source>#external programs list. example:
opera=opera %s</source>
        <translation type="vanished">#список внешних программ. пример:
opera=opera %s</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="8249"/>
        <source>%1 is not found&lt;br&gt;</source>
        <translation>%1 не найден&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1748"/>
        <source>Toggle word wrap</source>
        <translation>Переключить перенос строк</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1749"/>
        <source>Hide error marks</source>
        <translation>Скрыть помеченное как ошибки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1750"/>
        <source>Toggle fullscreen</source>
        <translation>Переключить полноэкранный режим</translation>
    </message>
    <message>
        <source>Preview with default browser</source>
        <translation type="vanished">Просмотр в браузере по умолчанию</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1762"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1452"/>
        <source>Scripts</source>
        <translation>Скрипты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1759"/>
        <source>NEWS</source>
        <translation>Новости</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5754"/>
        <source>Home</source>
        <translation>Домой</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5757"/>
        <source>Refresh</source>
        <translation>Освежить</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5810"/>
        <source>Save as</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2694"/>
        <source>Use traditional File Save/Open dialogs</source>
        <translation>Использовать традиционные окна Открыть/Сохранить</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5116"/>
        <source>templates</source>
        <translation>шаблоны</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5117"/>
        <source>snippets</source>
        <translation>сниппеты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5118"/>
        <source>scripts</source>
        <translation>скрипты</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6054"/>
        <source>Are you sure to delete
%1?</source>
        <translation>Вы уверены, что хотите удалить
%1?</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5741"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5751"/>
        <source>Go</source>
        <translation>Перейти</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1661"/>
        <source>Create new directory</source>
        <translation>Создать новый каталог</translation>
    </message>
    <message>
        <source>Quotes to facing quotes</source>
        <translation type="obsolete">Кавычки в кавычки ёлочкой</translation>
    </message>
    <message>
        <source>Override locale</source>
        <translation type="vanished">Перекрыть локаль</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1255"/>
        <source>Save session</source>
        <translation>Сохранить сессию</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1277"/>
        <source>Sessions</source>
        <translation>Сессии</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1517"/>
        <source>Enumerate</source>
        <translation>Нумеровать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1751"/>
        <source>Stay on top</source>
        <translation>Поверх других окон</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2668"/>
        <source>Restore the last session on start-up</source>
        <translation>Загружать последнюю сессию при запуске</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2759"/>
        <source>Common</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3902"/>
        <source>chars: %1&lt;br&gt;chars without spaces: %2&lt;br&gt;lines: %3&lt;br&gt;author&apos;s sheets: %4</source>
        <translation>символов: %1&lt;br&gt;символов без пробелов: %2&lt;br&gt;строк: %3&lt;br&gt;авторских листов: %4</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1240"/>
        <source>Last closed file</source>
        <translation>Последний закрытый файл</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1383"/>
        <source>Preview selected color</source>
        <translation>Смотреть выделенный цвет</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1738"/>
        <source>Palettes</source>
        <translation>Палитры</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2563"/>
        <source>Highlight current line</source>
        <translation>Подсвечивать текущую строку</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2568"/>
        <source>Highlight paired brackets</source>
        <translation>Подсвечивать парные скобки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5722"/>
        <source>&lt;span style=&quot;%1&quot;&gt;COLOR SAMPLE&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;%1&quot;&gt;ОБРАЗЕЦ ЦВЕТА&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1647"/>
        <source>Fm</source>
        <translation>Фп</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1578"/>
        <source>Remove from dictionary</source>
        <translation>Удалить из словаря</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1658"/>
        <source>File operations</source>
        <translation>Действия над файлами</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1662"/>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1663"/>
        <source>Delete file</source>
        <translation>Удалить файл</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1666"/>
        <source>File information</source>
        <translation>Сведения о файле</translation>
    </message>
    <message>
        <source>MD5 checksum</source>
        <translation type="vanished">Проверочная сумма MD5</translation>
    </message>
    <message>
        <source>MD4 checksum</source>
        <translation type="vanished">Проверочная сумма MD4</translation>
    </message>
    <message>
        <source>SHA1 checksum</source>
        <translation type="vanished">Проверочная сумма SHA1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1721"/>
        <source>Create web gallery</source>
        <translation>Создать веб-галерею</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1724"/>
        <source>Go to home dir</source>
        <translation>В домашний каталог</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1725"/>
        <source>Refresh current dir</source>
        <translation>Обновить текущий каталог</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2609"/>
        <source>Show margin at</source>
        <translation>Показывать границу на</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1716"/>
        <location filename="../rvln.cpp" line="2960"/>
        <source>Images</source>
        <translation>Картинки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="471"/>
        <source>logmemo</source>
        <translation>логмемо</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="491"/>
        <source>famous input field</source>
        <translation>знаменитое поле ввода</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1670"/>
        <source>Count lines in selected files</source>
        <translation>Подсчитать строки в выбранных файлах</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1727"/>
        <source>Select by regexp</source>
        <translation>Отметить по регэкспу</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1728"/>
        <source>Deselect by regexp</source>
        <translation>Снять выделение по регэкспу</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1744"/>
        <source>Profiles</source>
        <translation>Профили</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1747"/>
        <source>Save profile</source>
        <translation>Сохранить профиль</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2549"/>
        <source>Show line numbers</source>
        <translation>Показывать номера строк</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2932"/>
        <source>Web gallery options</source>
        <translation>Настройки веб-галереи</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2936"/>
        <source>Size of the side</source>
        <translation>Размер стороны</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2937"/>
        <source>Link options</source>
        <translation>Настройки ссылки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2938"/>
        <source>Columns per row</source>
        <translation>Столбцов в ряду</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2778"/>
        <source>Date and time</source>
        <translation>Дата и время</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="398"/>
        <location filename="../rvln.cpp" line="504"/>
        <source>The famous input field. Use for search/replace, function parameters</source>
        <translation>Знаменитое поле ввода (ЗПВ) - для поиска и замены, для параметров функций</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="814"/>
        <source>&lt;b&gt;TEA %1&lt;/b&gt; by Peter Semiletov, tea@list.ru&lt;br&gt;Sites: semiletov.org/tea and tea.ourproject.org&lt;br&gt;Git: github.com/psemiletov/tea-qt&lt;br&gt;AUR: https://aur.archlinux.org/packages/tea-qt/&lt;br&gt;VK: vk.com/teaeditor&lt;br&gt;read the Manual under the &lt;i&gt;Manual&lt;/i&gt; tab!</source>
        <translation>&lt;b&gt;TEA %1&lt;/b&gt; от Петра Семилетова, tea@list.ru&lt;br&gt;Сайты: semiletov.org/tea and tea.ourproject.org&lt;br&gt;Гит: github.com/psemiletov/tea-qt&lt;br&gt;AUR: https://aur.archlinux.org/packages/tea-qt/&lt;br&gt;ВК: vk.com/teaeditor&lt;br&gt;читайте Руководство на одноименной вкладке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2792"/>
        <source>Spell checking</source>
        <translation>Проверка правописания</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2799"/>
        <source>Spell checker engine</source>
        <translation>Движок проверки правописания</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2808"/>
        <source>Hunspell dictionaries directory</source>
        <translation>Каталог со словарями для Hunspell</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2815"/>
        <location filename="../rvln.cpp" line="2839"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3342"/>
        <source>Print document</source>
        <translation>Печатать документ</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3507"/>
        <source>elapsed milliseconds: %1</source>
        <translation>прошло миллисекунд: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5195"/>
        <location filename="../rvln.cpp" line="5363"/>
        <location filename="../rvln.cpp" line="6024"/>
        <location filename="../rvln.cpp" line="7640"/>
        <source>Enter the name</source>
        <translation>Введите имя</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5196"/>
        <location filename="../rvln.cpp" line="5364"/>
        <location filename="../rvln.cpp" line="6025"/>
        <location filename="../rvln.cpp" line="6993"/>
        <location filename="../rvln.cpp" line="7641"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5197"/>
        <source>new_directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5758"/>
        <source>Operations</source>
        <translation>Действия</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6026"/>
        <source>new</source>
        <translation>новый</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1671"/>
        <source>Full info</source>
        <translation>Полные сведения</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="4587"/>
        <source>#external programs list. example:
opera=&quot;C:\Program Files\Opera\opera.exe &quot; &quot;%s&quot;</source>
        <translation>#список внешних программ. вот пример:
opera=&quot;C:\Program Files\Opera\opera.exe &quot; &quot;%s&quot;</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6362"/>
        <source>file name: %1</source>
        <translation>имя файла: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6363"/>
        <source>size: %1 kbytes</source>
        <translation>размер: %1 kbytes</translation>
    </message>
    <message>
        <source>created: %1</source>
        <translation type="vanished">создан: %1</translation>
    </message>
    <message>
        <source>modified: %1</source>
        <translation type="vanished">изменен: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1259"/>
        <source>File actions</source>
        <translation>Действия над файлом</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1260"/>
        <source>Reload</source>
        <translation>Перечитать</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1261"/>
        <location filename="../rvln.cpp" line="6438"/>
        <source>Reload with encoding</source>
        <translation>Перечитать в кодировке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3256"/>
        <source>you can put here notes, etc</source>
        <translation>вы можете заносить сюда всякие заметки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1532"/>
        <source>Analyze</source>
        <translation>Анализ</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6550"/>
        <source>total / unique: %1</source>
        <translation>всего / уникальных: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6551"/>
        <source>words unique: %1</source>
        <translation>слов, не считая повторы: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6552"/>
        <source>words total: %1</source>
        <translation>слов всего: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6553"/>
        <source>text analysis of: %1</source>
        <translation>анализ текста для: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6554"/>
        <source>UNITAZ: UNIverlsal Text AnalyZer</source>
        <translation>УНИТАЗ: УНИверсальный Текстовый АналиЗатор</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1416"/>
        <source>Whole words</source>
        <translation>Целые слова</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1412"/>
        <source>Case sensitive</source>
        <translation>Чуткость к регистру</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1540"/>
        <source>UNITAZ quantity sorting</source>
        <translation>УНИТАЗ с сортировкой по количеству</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1541"/>
        <source>UNITAZ sorting alphabet</source>
        <translation>УНИТАЗ с сортировкой по алфавиту</translation>
    </message>
    <message>
        <source>tune</source>
        <translation type="obsolete">ладить</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="3417"/>
        <location filename="../rvln.cpp" line="6506"/>
        <location filename="../rvln.cpp" line="6858"/>
        <location filename="../rvln.cpp" line="7388"/>
        <location filename="../rvln.cpp" line="7543"/>
        <source>%p% completed</source>
        <translation>%p% выполнено</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5855"/>
        <source>&lt;b&gt;Bookmarks&lt;/b&gt;</source>
        <translation>&lt;b&gt;Закладки&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1638"/>
        <source>Focus the Famous input field</source>
        <translation>Фокус в Знаменитое поле ввода</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1168"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1005"/>
        <source>This file is open in the read-only mode. You can save it with another name using &lt;b&gt;Save as&lt;/b&gt;</source>
        <translation>Файл открыт в режиме только для чтения. Можете сохранить его под другим именем через &lt;b&gt;Сохранить как&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1538"/>
        <source>Count the substring</source>
        <translation>Подсчитать вхождение подстроки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1539"/>
        <source>Count the substring (regexp)</source>
        <translation>Подсчитать вхождение подстроки (регэксп)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6097"/>
        <source>MD4 checksum for %1 is %2</source>
        <translation>Проверочная сумма MD4 для %1 такова: %2</translation>
    </message>
    <message>
        <source>SHA1 checksum for %1 is %2</source>
        <translation type="vanished">Проверочная сумма SHA1 для %1 такова: %2</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6549"/>
        <source>total to unique per cent diff: %1</source>
        <translation>всего к уникальным, проценты: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6715"/>
        <source>%1 number of occurrences of %2 is found</source>
        <translation>%1 вхождений %2 найдено</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6083"/>
        <source>MD5 checksum for %1 is %2</source>
        <translation>Проверочная сумма MD5 для %1 такова: %2</translation>
    </message>
    <message>
        <source>&lt;b&gt;TEA %1 @ http://semiletov.org/tea&lt;/b&gt;&lt;br&gt;by Peter Semiletov (e-mail: tea@list.ru, site: semiletov.org)&lt;br&gt;read the Manual under the &lt;i&gt;Learn&lt;/i&gt; tab!</source>
        <translation type="obsolete">&lt;b&gt;TEA %1 @ http://semiletov.org/tea&lt;/b&gt;&lt;br&gt;Петр Семилетов (мыло: tea@list.ru, сайт: semiletov.org)&lt;br&gt;читайте руководство на вкладе &lt;i&gt;Узнать&lt;/i&gt;!</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1265"/>
        <source>Set old Mac end of line (CR)</source>
        <translation>Установить старый Маковский конец строки (CR)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1285"/>
        <source>Do not add to recent</source>
        <translation>Не добавлять в Последние файлы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1306"/>
        <source>Block start</source>
        <translation>Начало блока</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1307"/>
        <source>Block end</source>
        <translation>Конец блока</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1308"/>
        <source>Copy block</source>
        <translation>Копировать блок</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1309"/>
        <source>Paste block</source>
        <translation>Вставить блок</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1310"/>
        <source>Cut block</source>
        <translation>Вырезать блок</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1407"/>
        <source>Mark all found</source>
        <translation>Пометить всё найденное</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1552"/>
        <source>Remove trailing spaces</source>
        <translation>Удалить хвостовые пробелы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1553"/>
        <source>Escape regexp</source>
        <translation>Escape regexp</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1558"/>
        <source>Quotes</source>
        <translation>Кавычки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1561"/>
        <source>Straight to double angle quotes</source>
        <translation>Прямые двойные в елочкой</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1562"/>
        <source>Straight to curly double quotes</source>
        <translation>Прямые двойные в нижние и верхние</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1701"/>
        <source>ZIP</source>
        <translation>ZIP</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1706"/>
        <source>Create new ZIP</source>
        <translation>Создать новый ZIP</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1707"/>
        <source>Add to ZIP</source>
        <translation>Добавить в ZIP</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1708"/>
        <source>Save ZIP</source>
        <translation>Сохранить ZIP</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1719"/>
        <source>Scale by side</source>
        <translation>Масштабировать по стороне</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1720"/>
        <source>Scale by percentages</source>
        <translation>Масштабировать в процентах</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1752"/>
        <source>Darker</source>
        <translation>Темнее</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2426"/>
        <source>Classic</source>
        <translation>Классика</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2426"/>
        <source>Docked</source>
        <translation>Стыковка</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2429"/>
        <source>UI mode (TEA restart needed)</source>
        <translation>Режим интерфейса (ТИА надо будет перезапустить)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2450"/>
        <source>UI language (TEA restart needed)</source>
        <translation>Язык интерфейса (ТИА надо будет перезапустить)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2461"/>
        <source>UI style (TEA restart needed)</source>
        <translation>Стиль интрефейса (ТИА надо будет перезапустить)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2593"/>
        <source>Cursor center on scroll</source>
        <translation>Курсор по центру при прокрутке</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2599"/>
        <source>Cursor blink time (msecs, zero is OFF)</source>
        <translation>Время мигания курсора (мс, ноль - выключить)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2634"/>
        <source>Interface</source>
        <translation>Интерфейс</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2676"/>
        <source>Use Enca for charset detection</source>
        <translation>Использовать Enca для определения кодировки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2715"/>
        <source>Charset for file open from command line</source>
        <translation>Кодировка для открытия файла из командной строки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2833"/>
        <source>Aspell directory</source>
        <translation>Папка Aspell</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2899"/>
        <source>Image conversion output format</source>
        <translation>Выходной формат преобразованных картинок</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2903"/>
        <location filename="../rvln.cpp" line="2916"/>
        <source>Scale images with bilinear filtering</source>
        <translation>Масштабировать картинки с билинейным фильтром</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2908"/>
        <source>Output images quality</source>
        <translation>Выходное качество картинок</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2921"/>
        <source>Zip directory with processed images</source>
        <translation>Упаковать каталог с преобразованными картинками</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5119"/>
        <source>tables</source>
        <translation>таблицы</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5365"/>
        <source>new_session</source>
        <translation>новая сессия</translation>
    </message>
    <message>
        <source>manage</source>
        <translation type="obsolete">зырить</translation>
    </message>
    <message>
        <source>It seems that %1 contains TEA plugin
Do you want to install it?</source>
        <translation type="obsolete">Кажется, что %1 содержит плагин для TEA\nВы хотите установить его?</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6992"/>
        <source>Enter the archive name</source>
        <translation>Введите имя архива</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6994"/>
        <source>new_archive</source>
        <translation>new_archive</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1323"/>
        <source>Indent (tab)</source>
        <translation>Отступ (tab)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1324"/>
        <source>Un-indent (shift+tab)</source>
        <translation>Отменить отступ (shift-tab)</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1726"/>
        <source>Preview image</source>
        <translation>Смотреть картинку</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2578"/>
        <source>Use spaces instead of tabs</source>
        <translation>Использовать пробелы вместо табов</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2573"/>
        <source>Automatic indent</source>
        <translation>Автоматический отступ</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2665"/>
        <source>Automatic preview images at file manager</source>
        <translation>Авто-показ картинок в файловом приказчике</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="2590"/>
        <source>Tab width in spaces</source>
        <translation>Ширина таба в пробелах</translation>
    </message>
    <message>
        <source>Use wrap setting from highlighting module</source>
        <translation type="vanished">Брать настройку переноса строк из модуля подсветки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1479"/>
        <source>Sort case insensitively</source>
        <translation>Сортировать без учёта регистра</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6370"/>
        <source>bits per sample: %1</source>
        <translation>разрядность: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6371"/>
        <source>number of channels: %1</source>
        <translation>количество каналов: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6372"/>
        <source>sample rate: %1</source>
        <translation>частота оцифровки: %1</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="6374"/>
        <source>RMS for all channels: %1 dB</source>
        <translation>RMS для всех каналов: %1 dB</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1366"/>
        <source>Underline</source>
        <translation>Подчеркнутый</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="1741"/>
        <source>Highlighting mode</source>
        <translation>Режим подсветки</translation>
    </message>
    <message>
        <location filename="../rvln.cpp" line="5120"/>
        <source>configs</source>
        <translation>конфиги</translation>
    </message>
</context>
</TS>
